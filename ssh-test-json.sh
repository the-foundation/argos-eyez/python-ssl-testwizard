TARGET="$1"
PORT="$2"

## ipv6 is not supported by the belowused scanner, so putting port into target is filtered
test -d /tmp/SSHScan/ && ( cd /tmp/SSHScan/;git pull &>/dev/null )  || git clone https://github.com/evict/SSHScan /tmp/SSHScan/ &>/dev/null;
cd /tmp/; test -f SSHScan/sshscan.py && (
 if [ -z "$PORT" ] ; then echo -n ;else PORT=":"$PORT;fi
# echo $TARGET $PORT
	ping -q -c4 -i1 -w 6 ${TARGET/:.*///} >/dev/null && scanres=$(python SSHScan/sshscan.py -t "${TARGET}${PORT}" |sed 's/\[.\]/|/g'|tr -d '\n'| sed 's/|/\n|/g' |grep -e etected -e "Target SSH Version" | sed 's/Detected the following//g;s/ \+/ /g'|grep -v "No weak"|grep "^| weak"|sed 's/^| weak /{"weak_/g;s/\( \|\)"\( \|\)/"/g;s/\(.*\) \(.*\)[^:]:/\1_\2:/;s/$/"},/g;s/: /:/g;s/:/":[ "/g;s/ "/"/g;s/},/]},/g;s/ /","/g'|tr -d '\n'|sed 's/,$/]}/g;s/^/{ "target":"'$TARGET'" , "ssh-keyscan": [/g' )
#echo "$scanres"
if [ -z "${scanres}" ];then echo '{"target":"'$TARGET'" , "statuscode": "000" , "message": "CONNECT OR SCAN FAILED OR TIMED OUT" }' ;else
	echo ${scanres}| grep -q -e weak -e WEAK && ( 	echo ${scanres} ) || (echo '{"target":"'$TARGET'" , "statuscode": "200" ,  "status": "OK" , "message": "OK" }' )  ;fi )
