"""
## argos-eyez python ssl test and verify  library

# requires a recent enough python with idna support in socket
# pyopenssl, cryptography and idna


Constants around Mail Protocols, Mail Ports, Mail Commands and Socket Connectivity.
"""

from cachetools import TTLCache
## this cache is used for things like mailserver tests since they are rather costy ( up to 2-4) minutes / host
### after half an hour , somebody might have taken over a mailbox of your system and already provoke bounces
### which should send separate alerts ..
cert_cache = TTLCache(maxsize=50, ttl=120)

def __main__():
    return 0

global_debug=0

POP_STANDARD = 110
POP_IMPLICIT_SSL = 995
POP_USER = b'USER '
POP_PASS = b'PASS '

SMTP_STANDARD = 25
SMTP_IMPLICIT_SSL = 465
SMTP_STARTTLS_SSL = 587
SMTP_EHLO = b'EHLO gmail.com \r\n'
SMTP_AUTH = b'AUTH LOGIN \r\n'

IMAP_STANDARD = 143
IMAP_IMPLICIT_SSL = 993
IMAP_RAND_STRING = b'A1 '
IMAP_CAPABILITY = b'CAPABILITY \r\n'
IMAP_LOGIN = b'LOGIN '

STARTTLS_COMMAND = b'STARTTLS \r\n'

import sys
import re
import logging
import pprint
logger = logging.getLogger('ssl test wizard')

import datetime
import smtplib
import ssl

from OpenSSL import SSL
from cryptography import x509
from cryptography.x509.oid import NameOID
import idna


def create_tls_context(TLSSTRENGTH):

    #CREATE a CONTEXT that we can then update
    context = ssl.SSLContext(protocol=ssl.PROTOCOL_TLS)

    if TLSSTRENGTH == "tls1_2":
        context = ssl.SSLContext(protocol=ssl.PROTOCOL_TLSv1_2)

    elif TLSSTRENGTH == "tls1_1":
        context = ssl.SSLContext(protocol=ssl.PROTOCOL_TLSv1_1)

    elif TLSSTRENGTH == "tls1":
        context = ssl.SSLContext(protocol=ssl.PROTOCOL_TLSv1)

    else:
        if global_debug>1:
            print("Valid TLS Protocol Not Found: Needs to be in OpenSSL format: tls_1, tls_1_1 tls_2")
        return

    context.verify_mode = ssl.CERT_REQUIRED
    context.check_hostname = True
    context.load_default_certs()
    if global_debug > 1:
        print("TLS Protocol Specified: {}".format(TLSSTRENGTH))
    return context

#This was modified from https://serverlesscode.com/post/ssl-expiration-alerts-with-lambda/
# but original exampe was total crap, failing on invalid certs..
def ssl_expiry_datetime(hostname,port):
    import socket
    ssl_date_fmt = r'%b %d %H:%M:%S %Y %Z'
    if global_debug >0 :
        print("connect_ssl:",hostname,port)
    #context=SSL.Context(SSL.SSLv23_METHOD)
    context = ssl.create_default_context()
    context.check_hostname = False
    #context.verify_mode = SSL.VERIFY_NONE
    #context= ssl.SSLContext(ssl.PROTOCOL_SSLv23)
    #context.verify_mode = ssl.CERT_NONE
    conn = context.wrap_socket(
        socket.socket(socket.AF_INET),
        server_hostname=hostname,

    )
    # 3 second timeout because Lambda has runtime limitations
    #conn.settimeout(3.0)
    try:
        conn.connect((hostname, port))
        ssl_info = conn.getpeercert()
        #print(ssl_info)
        # parse the string from the certificate into a Python datetime object
        if "notAfter" in ssl_info:
            return datetime.datetime.strptime(ssl_info['notAfter'], ssl_date_fmt)
        else:
            return datetime.datetime(1970, 1, 1)
    except:
        return datetime.datetime(1970, 1, 1)



def ssl_valid_time_remaining(hostname,port):
    """Get the number of days left in a cert's lifetime."""
    expires = ssl_expiry_datetime(hostname,port)
    if global_debug > 1:
        print(expires)
        logger.debug( "SSL cert for %s expires at %s", hostname, expires.isoformat() )
    return expires - datetime.datetime.utcnow()


def ssl_valid_days_remaining(hostname,port):
    """Get the number of days left in a cert's lifetime."""
    remaining = ssl_valid_time_remaining(hostname,port)
    if global_debug > 1:
    #logger.debug( "SSL cert for %s %s expires at %s", hostname, port,  remaining.isoformat() )
        logger.debug( "SSL cert for %s %s expires at %s", hostname, port,  remaining )
    return remaining.days

def ssl_expires_in(hostname,port, buffer_days=21):
    """Check if `hostname` SSL cert expires is within `buffer_days`.

    Raises `AlreadyExpired` if the cert is past due
    """
    if global_debug > 1:
        print("remaintestssl")
    remaining = ssl_valid_time_remaining(hostname,port)
    if global_debug >  1:
        print(remaining)
    # if the cert expires in less than two weeks, we should reissue it
    if remaining < datetime.timedelta(days=0):
        # cert has already expired - uhoh!
        raise AlreadyExpired("Cert expired %s days ago" % remaining.days)
    elif remaining < datetime.timedelta(days=buffer_days):
        # expires sooner than the buffer
        return False
    else:
        # everything is fine
        return True


### get a starttls certificate (simple) ## sourced here https://serverfault.com/questions/131627/how-to-inspect-remote-smtp-servers-tls-certificate

#def ssl_get_cert_startls(hostname,port):
#    connection = smtplib.SMTP()
#    connection.connect('hostname',port)
#    connection.starttls()
#print ssl.DER_cert_to_PEM_cert(connection.sock.getpeercert(binary_form=True))


"""
STARTTLS Connection: This is a standard IPV4 socket that passes a STARTTLS Commands
STARTTLS REFERENCE: https://www.fastmail.com/help/technical/ssltlsstarttls.html
STARTTLS PYTHON WITH SOCKETS CODE: https://stackoverflow.com/questions/12593944/how-to-start-tls-on-an-active-connection-in-python
THE code above is Python2 ssl so the code below uses SSL contexts compatible with Python3 Sockets and SSL
NOTE: some protocols such as IMAP and SMTP require extra data to be sent or formatting
params:
HOST - host to connect to - this should be a string
PORT - port to connect to - this should be an INT
PROTOCOl - protocol to test - this should be a string
TLSSTRENGTH - in OpenSSL syntax
"""

def starttls_connection(HOST,PORT,PROTOCOL="smtp",TLSSTRENGTH="tls1_2"):
    if global_debug ==1:
        print("Connecting to host: {}  on Port Number {} using STARTTLS \r\n".format(HOST,PORT))
#    with socket.socket(AF_INET, socket.SOCK_DGRAM) as client:
    import socket
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    SOCKET_TIMEOUT=5
    client.settimeout(SOCKET_TIMEOUT)
    context = create_tls_context(TLSSTRENGTH)

    try:
        if global_debug ==1:
            print ("try_connect",socket.gethostbyname(HOST),PORT)
        client.connect((socket.gethostbyname(HOST), PORT))
        if global_debug ==1:
            print("after connect")
        data = client.recv(1024)
        if global_debug ==1:
            print("inital data:",data)
        # SMTP NEEDS A EHLO MESSAGE BEFORE STARTTLS AND OTHER COMMANDS
        # IMAP NEEDS A RANDOM STRING FOR THE STARTTLS COMMAND
        # POP3 - investigate but for now assume it's just a straight forward stattls

        if PROTOCOL=="smtp":
            client.send(SMTP_EHLO)
            data = client.recv(1024)
            #print('Received', repr(data))
            client.send(STARTTLS_COMMAND)
            data = client.recv(1024)
            if global_debug ==1:
                print('Response from STARTTLS_COMMAND:', repr(data))

        if PROTOCOL=="imap":
            client.send(IMAP_RAND_STRING + STARTTLS_COMMAND)
            data = client.recv(1024)
            if global_debug ==1:
                print('Response from STARTTLS_COMMAND', repr(data))

        if PROTOCOL=="pop":
            client.send(STARTTLS_COMMAND)
            data = client.recv(1024)
            if global_debug ==1:
                print('Response from STARTTLS_COMMAND', repr(data))



        secure_client = context.wrap_socket(client,server_hostname=HOST)

        # SMTP NEEDS A EHLO MESSAGE BEFORE STARTTLS AND OTHER COMMANDS

        if PROTOCOL=="smtp":
            secure_client.send(SMTP_EHLO)
            data = secure_client.recv(1024)
            #print('Results of SMTP EHLO', repr(data),'\r\n')
        client.close()
        #pass the secure client to the write protocol conversation handler
        return secure_client.getpeercert()
        #print_cipher_certificate(secure_client)
        #decide_protocol_handler(secure_client,PROTOCOL)

    except Exception as e:
        if global_debug == 1:
            print("Connection Could Not Be Established \r\n")
            print(e)
            return {'failed': e }
        else:
            return { 'failed', e }


#This was modified from https://serverlesscode.com/post/ssl-expiration-alerts-with-lambda/
def starttls_expiry_datetime(hostname,port,protocol="smtp",tls_version="tls1_2"):
    starttls_date_fmt = r'%b %d %H:%M:%S %Y %Z'


    starttls_info = starttls_connection(hostname,port,protocol,tls_version)
    # parse the string from the certificate into a Python datetime object
    if "failed" in starttls_info:
        return starttls_info
    else:
        return datetime.datetime.strptime(starttls_info['notAfter'], starttls_date_fmt)


def starttls_valid_time_remaining(hostname,port,protocol="smtp",tls_version="tls1_2"):
    """Get the number of days left in a cert's lifetime."""
    expires = starttls_expiry_datetime(hostname,port,protocol,tls_version)
    if not type(expires) is datetime.datetime:
        return expires
    else:
        if global_debug == 1:
            logger.debug("STARTTLS CERT for %s expires at %s",hostname, expires.isoformat())
        return expires - datetime.datetime.utcnow()



def starttls_valid_days_remaining(hostname,port,protocol="smtp",tls_version="tls1_2"):
    """Get the number of days left in a cert's lifetime."""
    remaining = starttls_valid_time_remaining(hostname,port,protocol,tls_version)
    if not type(remaining) is datetime.datetime:
        return remaining
    else:
        if global_debug == 1:
            logger.debug("STARTTLS CERT for %s %s expires at %s", hostname, port,  remaining.isoformat() )
        return remaining.days

def starttls_expires_in(hostname,port, protocol="smtp",tls_version="tls1_2", buffer_days=21):
    """Check if `hostname` STARTTLS CERT expires is within `buffer_days`.

    Raises `AlreadyExpired` if the cert is past due
    """
    remaining = starttls_valid_time_remaining(hostname,port,protocol,tls_version)
    if not type(remaining) is datetime.timedelta:
        return True
    else:
        if global_debug == 1:
            print("have timedelta",remaining)
        # if the cert expires in less than two weeks, we should reissue it
        if remaining < datetime.timedelta(days=0):
            # cert has already expired - uhoh!
            raise AlreadyExpired("Cert expired %s days ago" % remaining.days)
            return true
        elif remaining < datetime.timedelta(days=buffer_days):
            # expires sooner than the buffer
            return False
        else:
            # everything is fine
            return True


### the following functions are inspired from here : https://gist.github.com/gdamjan/55a8b9eec6cf7b771f92021d93b87b2c

from socket import socket
from collections import namedtuple

HostInfo = namedtuple(field_names='cert hostname peername', typename='HostInfo')

def verify_cert(cert, hostname):
    # verify notAfter/notBefore, CA trusted, servername/sni/hostname
    cert.has_expired()
    # service_identity.pyopenssl.verify_hostname(client_ssl, hostname)
    # issuer

def get_certificate(hostname, port):
    hostname_idna = idna.encode(hostname)
    if global_debug == 1:
        print("hostname_idna",hostname_idna)
    cachename=hostname_idna+'_'+str(port)
    try :
        if not cachename in cert_cache:
            sock = socket()
            #sock = socket.create_connection((hostname_idna,port))
            SOCKET_TIMEOUT=5
            #sock.settimeout(SOCKET_TIMEOUT)
            #if global_debug == 1:
            #    print("after settimeout")
            sock.connect((hostname, port))
            peername = sock.getpeername()
            if global_debug ==1:
                print("PEERNAME ",peername)
            ctx = SSL.Context(SSL.SSLv23_METHOD) # most compatible
            ctx.check_hostname = False
            ctx.verify_mode = SSL.VERIFY_NONE
            sock_ssl = SSL.Connection(ctx, sock)

            sock_ssl.set_connect_state()
            sock_ssl.set_tlsext_host_name(hostname_idna)
            sock_ssl.do_handshake()
            if global_debug == 1:
                print("after handshake")
            cert = sock_ssl.get_peer_certificate()
            cert_cache[cachename] = cert.to_cryptography()
            sock_ssl.close()
            sock.close()
        else:
            peername="cache"
    except:
        cert_cache[cachename]=''
        peername='failed'
        pass


#    return HostInfo(cert=crypto_cert, peername=peername, hostname=hostname)
    return HostInfo(cert=cert_cache[cachename], peername=peername, hostname=hostname)

def get_alt_names(cert):
    try:
        ext = cert.extensions.get_extension_for_class(x509.SubjectAlternativeName)
        return ext.value.get_values_for_type(x509.DNSName)
    except x509.ExtensionNotFound:
        return None

def get_common_name(cert):
    try:
        names = cert.subject.get_attributes_for_oid(NameOID.COMMON_NAME)
        return names[0].value
    except x509.ExtensionNotFound:
        return None

def get_issuer(cert):
    try:
        names = cert.issuer.get_attributes_for_oid(NameOID.COMMON_NAME)
        return names[0].value
    except x509.ExtensionNotFound:
        return None


def print_basic_info(hostinfo):
    s = '''> {hostname} < ... {peername}
    \tcommonName: {commonname}
    \tSAN: {SAN}
    \tissuer: {issuer}
    \tnotBefore: {notbefore}
    \tnotAfter:  {notafter}
    '''.format(
            hostname=hostinfo.hostname,
            peername=hostinfo.peername,
            commonname=get_common_name(hostinfo.cert),
            SAN=get_alt_names(hostinfo.cert),
            issuer=get_issuer(hostinfo.cert),
            notbefore=hostinfo.cert.not_valid_before,
            notafter=hostinfo.cert.not_valid_after
    )
    print(s)

def ssl_match_hostname(cert, hostname):
        """Verify that *cert* (in decoded format as returned by
        SSLSocket.getpeercert()) matches the *hostname*.  RFC 2818 and RFC 6125
        rules are followed, but IP addresses are not accepted for *hostname*.

        ssl.CertificateError is raised on failure. On success, the function
        returns nothing.
        """
        if not cert:
            raise ValueError("empty or no certificate")
        dnsnames = []
        if global_debug==1:
            print("cert:")
            print(cert)
        try:
            san=cert["subjectAltName"]
            if global_debug==1:
                pp = pprint.PrettyPrinter(indent=4)
                pp.pprint(cert["subjectAltName"])
        except:
            if global_debug == 1:
                print("SAN=CERT.subjectAltName failed")
            san={}
            pass
        #san = cert.get('subjectAltName', ())
        #san=get_alt_names(hostinfo.cert)
        for key, alt_name in san:
             if key == 'DNS':
                if ssl._dnsname_match(alt_name, hostname):
                    return True
        #for alt_name in get_alt_names(cert):
        #    if ssl._dnsname_match(alt_name, hostname):
        #        return True
                dnsnames.append(alt_name)
        if not dnsnames:
            # The subject is only checked when there is no dNSName entry
            # in subjectAltName
            try:
                subjects=cert["subject"]
            except:
                subjects={}
            #for sub in cert.get('subject', ()):
            for sub in subjects:
                for key, value in sub:
                    # XXX according to RFC 2818, the most specific Common Name
                    # must be used.
                    if key == 'commonName':
                        if _dnsname_match(value, hostname):
                            return True
                        dnsnames.append(value)
        if len(dnsnames) > 1:
            raise ssl.CertificateError("hostname %r "
                "doesn't match either of %s"
                % (hostname, ', '.join(map(repr, dnsnames))))
        elif len(dnsnames) == 1:
            raise ssl.CertificateError("hostname %r "
                "doesn't match %r"
                % (hostname, dnsnames[0]))
        else:
            raise ssl.CertificateError("no appropriate commonName or "
                "subjectAltName fields were found")
            if global_debug==1:
                print("Searched in",cert," for ", hostname)


def starttls_match_hostname(cert, hostname):
        """Verify that *cert* (in decoded format as returned by
        SSLSocket.getpeercert()) matches the *hostname*.  RFC 2818 and RFC 6125
        rules are followed, but IP addresses are not accepted for *hostname*.

        ssl.CertificateError is raised on failure. On success, the function
        returns nothing.
        """
        if not cert:
            raise ValueError("empty or no certificate")
        if not "subjectAltName" in cert:
            raise ValueError("empty or no certificate")
        dnsnames = []
        if global_debug==1:
            pp = pprint.PrettyPrinter(indent=4)
        #pp.pprint(cert)
            pp.pprint(cert["subjectAltName"])
        #san = cert.subjectAltName

        #san=get_alt_names(cert)
        for current_name in cert["subjectAltName"]:
            if current_name[0] == 'DNS':
                if ssl._dnsname_match(current_name[1], hostname):
                    return True
            dnsnames.append(current_name[1])
        #for alt_name in get_alt_names(cert):
        #    if ssl._dnsname_match(alt_name, hostname):
        #        return True
            dnsnames.append(current_name[1])
        if not dnsnames:
            # The subject is only checked when there is no dNSName entry
            # in subjectAltName
            for sub in cert["subjectAltName"]:
                for current_name in sub:
                    # XXX according to RFC 2818, the most specific Common Name
                    # must be used.
                    if current_name[0] == 'commonName':
                        if _dnsname_match(current_name[1], hostname):
                            return True
                        dnsnames.append(current_name[1])
        if len(dnsnames) > 1:
            raise ssl.CertificateError("hostname %r "
                "doesn't match either of %s"
                % (hostname, ', '.join(map(repr, dnsnames))))
        elif len(dnsnames) == 1:
            raise ssl.CertificateError("hostname %r "
                "doesn't match %r"
                % (hostname, dnsnames[0]))
        else:
            raise ssl.CertificateError("no appropriate commonName or "
                "subjectAltName fields were found")


def ssl_print_info(hostname, port):
    hostinfo = get_certificate(hostname, port)
    print_basic_info(hostinfo)

def ssl_get_cert(hostname,port):
    return get_certificate(hostname,port)


#def starttls_get_cert(hostname,port,protocol="smtp",tls_version="tls1_2"):
#    print(0)




def verify_mailserver_encryption(target,buffer_days=22):
    target=target.rstrip('.')
    mxssl={}
    mxerrors={}
    ################### VERIFY MAILSERVER HAS A CERTIFICATE FOR AT LEAST 21 DAYS TO Keep letsencrypt min date #####
    port=465
    if global_debug == 1:
        print("SSL Cert Days",target,port)
    try:
        days_till_expire=ssl_valid_days_remaining(target,port)
        if days_till_expire < buffer_days:
            mxerrors["ssldays_smtp_"+port]="Certificate on "+target+" : "+port+" will expire in "+days_till_expire+"days"
        if global_debug ==1:
            print(days_till_expire)
        mxssl["ssldays_smtp_465"]=days_till_expire
        #result=ssl_expires_in(target,port, 22)
    except:
        if global_debug == 1:
            print("FAILED in except")
        pass
####################################################################
    if global_debug == 1:
        print("verifying hostname SSL",target,port,target)
    try:
        hostinfo=get_certificate(target, port)
        if global_debug == 1:
            print("got hostinfo")
            print(hostinfo)
        result=ssl_match_hostname(hostinfo.cert,target)
        if global_debug == 1:
            print("got result")
            print(result)
            print("---")
        if result == False :
            mxssl["sslname_"+str(port)]="FAILED"
            mxerrors["sslname_"+str(port)]="FAILED hostname "+str(target)+" : "+str(port)
            if global_debug == 1:
                print(mxssl["sslname_465"])
    except:
        if global_debug == 1:
            print("FAILED in except")
        pass
#####################################################################
    for port in [ 25 , 587 ]:
        if global_debug == 1:
            print("startssl certificate dump",target,port)
        try:
            hostinfo=starttls_connection(target,port,"smtp")
            if len(hostinfo) < 0:
                try:
                    result=ssl_match_hostname(hostinfo,target)
                    hostinfo=starttls_connection(target,port,"smtp")
                    if global_debug == 1:
                        print("got hostinfo startls")
                        print(hostinfo)
                    result=ssl_match_hostname(hostinfo,target)
                    if global_debug == 1:
                        print("got result")
                        print(result)
                        print("---")
                    if result == False :
                        mxssl["sslname_"+str(port)]="FAILED"
                        mxerrors["sslname_"+str(port)]="FAILED hostname "+str(target)+" : "+str(port)
                        if global_debug == 1:
                            print(mxssl["sslname_"+str(port)])
                    if global_debug == 1:
                        print("starttls Cert Days",target)
                    try:
                        days_till_expire=starttls_valid_days_remaining(target,port, "smtp","tls1_2")
                        #print(days_till_expire)
                        #print(type(days_till_expire.days))
        #                if days_till_expire <  datetime.timedelta(days=21):
                        if days_till_expire.days <  22:
                            mxerrors["ssldays_smtp_"+str(port)]="Certificate on "+str(target)+" : "+str(port)+" will expire in "+str(days_till_expire)+"days"
                            if global_debug == 1:
                                print("Certificate on "+str(target)+" : "+str(port)+" will expire in "+str(days_till_expire)+"days")
                        #print("before set")
                        mxssl["ssldays_smtp_"+str(port)]=days_till_expire.days
                    except:
                        ###closed ports are OK
                        #mxerrors["sslname_"+str(port)]="Connection error on hostname "+str(target)+" : "+str(port)
                        pass
                except:
                    if global_debug == 1:
                        print("FAILED in except get startssldays")
                    pass
            else: ## connection error or port closed
            ###closed ports are OK
                #mxerrors["sslname_"+str(port)]="Connection error on  hostname "+str(target)+" : "+str(port)
                pass



            #print(days_till_expire)
            #print(buffer_days)
            #print("lalal")
##
        except:
            if global_debug == 1:
                print("FAILED in except get hostinfo")
            pass
    return { "mxssl": mxssl , "mxerrors": mxerrors }
